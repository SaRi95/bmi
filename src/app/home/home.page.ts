import { Component } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  height: number;
  weight: number;
  bmi: number;

  constructor() {
  this.height = 0;
  this.weight = 0;
  this.bmi = 0;

  }
  calculate() {
    this.bmi = this.weight / (Math.pow(this.height, 2));
  }
}



  
